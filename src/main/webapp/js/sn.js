$(document).ready(function(){
    $("#submitButton").click(function(){
        var query = $("#serialNumber").val();
        var data = $("#searchForm").serialize();
        var post_url = $("#searchForm").attr("action");
        $("#error").hide();
        $.ajax({
            url: post_url,
            method: "POST",
            data: data,
            success: function(data){
                $("#resultTitle").text("Результаты поиска " + query);
                var $target = $("#resultBody");
                $target.empty();
                $(data).each(function(index, item){
                    $target.append("<tr><td>" + item.code + "</td><td>" + item.name + "</td><td>" + item.serialNumber + "</td></tr>");
                });
                if (data.length == 0){
                    $("#searchResult").hide();
                    $("#error").html("Серийный номер <strong>\"" + query + "\"</strong> не найден").show();
                } else {
                    $("#searchResult").show();
                }
                $("#serialNumber").focus().select();
            }
        });
    });
});