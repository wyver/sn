package com.pentar.sn.model;

import javax.validation.constraints.NotNull;

/**
 * @author VKoulakov
 * @since 22.01.2015 19:54
 */
public class SearchForm {
    @NotNull
    private String serialNumber;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
