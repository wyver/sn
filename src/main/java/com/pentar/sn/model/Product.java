package com.pentar.sn.model;

/**
 * Created with IntelliJ IDEA.
 * User: vitaly
 * Date: 23.01.15
 * Time: 2:14
 * To change this template use File | Settings | File Templates.
 */
public class Product {
    private String code;
    private String name;
    private String serialNumber;

    public Product(String code, String name, String serialNumber) {
        this.code = code;
        this.name = name;
        this.serialNumber = serialNumber;
    }

    public Product() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (code != null ? !code.equals(product.code) : product.code != null) return false;
        if (name != null ? !name.equals(product.name) : product.name != null) return false;
        if (serialNumber != null ? !serialNumber.equals(product.serialNumber) : product.serialNumber != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = code != null ? code.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (serialNumber != null ? serialNumber.hashCode() : 0);
        return result;
    }

    public String getCode() {

        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
}
