package com.pentar.sn.controllers;

import com.pentar.sn.model.Product;
import com.pentar.sn.model.SearchForm;
import com.pentar.sn.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.validation.Valid;
import java.util.List;

/**
 * @author VKoulakov
 * @since 22.01.2015 19:31
 */
@Controller
public class MainController {
    @Autowired
    private SearchService searchService;

    @ModelAttribute
    protected SearchForm newForm(){
        return new SearchForm();
    }

    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public void index(){
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody List<Product> submit(@Valid SearchForm form){
        return searchService.findProducts(form.getSerialNumber());
    }
}
