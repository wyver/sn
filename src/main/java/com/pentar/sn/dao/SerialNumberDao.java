package com.pentar.sn.dao;

import com.pentar.sn.model.Product;

import java.util.List;

/**
 * @author VKoulakov
 * @since 23.01.2015 20:01
 */
public interface SerialNumberDao {
    List<Product> selectBySerialNumber(String serialNumber);
}
