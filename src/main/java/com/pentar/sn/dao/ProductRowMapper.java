package com.pentar.sn.dao;

import com.pentar.sn.model.Product;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
* @author VKoulakov
* @since 23.01.2015 20:15
*/
public class ProductRowMapper implements RowMapper<Product> {
    @Override
    public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
        Product product = new Product();
        product.setCode(rs.getString("Code"));
        product.setName(rs.getString("Name"));
        product.setSerialNumber(rs.getString("SerialNum"));
        return product;
    }
}
