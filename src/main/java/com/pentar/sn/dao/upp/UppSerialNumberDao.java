package com.pentar.sn.dao.upp;

import com.pentar.sn.dao.ProductRowMapper;
import com.pentar.sn.dao.SerialNumberDao;
import com.pentar.sn.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author VKoulakov
 * @since 23.01.2015 20:04
 */
@Repository
public class UppSerialNumberDao implements SerialNumberDao {

    private JdbcTemplate jdbcTemplate;

    private static final String QUERY = "select sn._Fld27955 as SerialNum," +
            "p._Fld2918 as Code, p._Description as Name" +
            "    from _InfoRg27953 sn join _Reference162 p on " +
            "p._IDRRef = sn._Fld27954RRef where sn._Fld27955=?";

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public List<Product> selectBySerialNumber(final String serialNumber) {
        return jdbcTemplate.query(QUERY, new PreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps) throws SQLException {
                ps.setString(1, serialNumber);
            }
        }, new ProductRowMapper());
    }

}
