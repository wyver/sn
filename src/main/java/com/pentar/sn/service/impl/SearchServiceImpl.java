package com.pentar.sn.service.impl;

import com.google.common.collect.ImmutableList;
import com.pentar.sn.dao.SerialNumberDao;
import com.pentar.sn.model.Product;
import com.pentar.sn.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vitaly
 * Date: 23.01.15
 * Time: 2:41
 * To change this template use File | Settings | File Templates.
 */
@Component
public class SearchServiceImpl implements SearchService{

    private List<Product> products = ImmutableList.<Product>builder()
            .add(new Product("MP1234", "Product1", "123456"))
            .add(new Product("MP1235", "Product2", "123457"))
            .add(new Product("MP1236", "Product3", "123458"))
            .build();

    @Autowired
    private SerialNumberDao serialNumberDao;

    @Override
    public List<Product> findProducts(final String serialNumber) {
        return serialNumberDao.selectBySerialNumber(serialNumber);
/*
        return Lists.newArrayList(Iterables.filter(products, new Predicate<Product>() {
            @Override
            public boolean apply(@Nullable Product product) {
                return product != null && product.getSerialNumber().startsWith(serialNumber);
            }
        }));
*/
    }
}
