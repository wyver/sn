package com.pentar.sn.service;

import com.pentar.sn.model.Product;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: vitaly
 * Date: 23.01.15
 * Time: 2:39
 * To change this template use File | Settings | File Templates.
 */
public interface SearchService {
    List<Product> findProducts(String serialNumber);
}
